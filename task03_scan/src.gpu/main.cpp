#pragma warning(disable:4996)

#include <iostream>
#include <iterator>
#include <sstream>
#include <vector>
#include <algorithm>
#include <functional>

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <ctime>

#include <cuda_runtime.h>
// Required to include CUDA vector types
//#include <vector_types.h>

// Golden CPU implementation
#include "golden.h"

using namespace std;

// forward declaration of cuda entry point function
extern "C" cudaError_t process(float* result, const float* source, int N, bool shared);


template <typename T>
void read_values(const string &s, vector<T> &values)
{
	istringstream iss;
	iss.str(s);
	copy(istream_iterator<T>(iss), istream_iterator<T>(), back_inserter(values));
}

template <typename T>
void print_matrix(const vector<T> &values, int M, int N, const string &name)
{
	cerr << "###########################" << endl;
	cerr << "### " << name << endl;
	cerr << "###########################" << endl;

	for (int i = 0; i < M; ++i)
	{
		for (int j = 0; j < N; ++j)
		{
			cerr << values[i * N + j] << ' ';
		}
		cerr << endl;
	}
}


void gen(int N)
{
  std::ostringstream ss;
  //ss << "input_" << N << ".txt";
	ss << "input.txt";
  string fileName = ss.str();

	srand((unsigned int)time(0));

  freopen(fileName.c_str(), "w", stdout);

  cout << N << endl;

  for (int i = 0; i < N; ++i)
  {
    cout << 10.f * rand() / RAND_MAX;
    cout << " ";
  }
}

bool float_equal_predicate(float a, float b)
{
	const float eps = (float)1e-1;
	return fabs(a - b) < eps;
}

int main(int argc, char **argv)
{
	typedef vector<float> Vecf;
	typedef vector<int>   Veci;
	string s;
	int N;

  gen(512);
  //return 0;

	if (freopen("input.txt", "r", stdin) == nullptr)
  {
    cout << "No input file found, exiting now." << endl;
    return 1;
  }
	//
	freopen("output.txt", "w", stdout);

	// read number of elements
	getline(cin, s);
  Veci values;
	read_values(s, values);
	assert(values.size() == 1);
  // number of elements
	N = values[0];

	// gpu implementation works only for small arrays
	assert(N <= 512);

	// read input elements
	getline(cin, s);
	Vecf elems;
	read_values(s, elems);
	assert(elems.size() == N);

  print_matrix(elems, 1, N, "Input elements");

	// create golden resulting array of same size
	Vecf result_golden(elems);

  // run golden implementation
  process_golden(result_golden.data(), elems.data(), N);

  print_matrix(result_golden, 1, N, "Inclusive scan result golden");

  // create resulting array of same size
  Vecf result(elems);
	// run the device part of the program
	cudaError_t cudaStatus = process(result.data(), elems.data(), N, false);
	if (cudaStatus != cudaSuccess)
	{
		cerr << "cuda execution failed!" << endl;
		return 1;
	}

  print_matrix(result, 1, N, "Inclusive scan result");

  // compare with golden
  // also may use std::mismatch()
  if (! std::equal(result_golden.begin(), result_golden.end(), result.begin(), float_equal_predicate))
  {
    cerr << "VERY BAD ERROR!!! CUDA IMPLEMENTATION IS INVALID!!!" << endl;
  }

	// write output matrix
	for (int i = 0; i < N; ++i)
	{
		cout << result[i] << ' ';
	}

	// cudaDeviceReset must be called before exiting in order for profiling and
	// tracing tools such as Nsight and Visual Profiler to show complete traces.
	cudaStatus = cudaDeviceReset();
  if (cudaStatus != cudaSuccess)
  {
    cerr << "cudaDeviceReset failed!" << endl;
    return 1;
  }

	return 0;
}

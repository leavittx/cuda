// My own cuda helper library
#include "gpulib.h"
// Cuda kernels implementation
#include "kernel.h"

// Cuda headers
#include <cuda_runtime.h>

__global__ void inclusiveScanNaiveKernel(const float *src, float *dst, int N)
{
  int index = blockIdx.x * blockDim.x + threadIdx.x;

	float sum = 0;
	for (int i = 0; i <= index; ++i)
	{
		sum += src[i];
	}

	dst[index] = sum;
}

__global__ void inclusiveScanKernel(const float *src, float *dst, int N)
{
  int index = blockIdx.x * blockDim.x + threadIdx.x;

	if (index >= N)
	{
		return;
	}

	dst[index] = src[index];
	__syncthreads();

	for (int offset = 1; offset < N; offset *= 2)
	{
		if (index >= offset)
		{
			dst[index] = dst[index] + dst[index - offset];
		}
		else
		{
			// nothing
		}
		__syncthreads();
	}
}


// Cuda entry point for convolution
extern "C" cudaError_t process(float* result, const float* source, int N, bool shared)
{
  uint blockSize, gridSize;
  size_t size = N;

  try
  {
    // Get CUDA devices count
    int deviceCount;
    THROW_ON_CUDA_ERROR(
      cudaGetDeviceCount(&deviceCount)
    );

    if (deviceCount == 0)
    {
      fprintf(stderr, "There are no available device(s) that support CUDA\n");
      return cudaErrorDevicesUnavailable;
    }
    else
    {
      fprintf(stderr, "Detected %d CUDA Capable device(s)\n", deviceCount);
    }

    // Choose which GPU to run on
    int device = gpuGetMaxGflopsDeviceId();
    THROW_ON_CUDA_ERROR(
      cudaSetDevice(device)
    );

    // Allocate GPU buffers for two vectors (one input, one output)
    CudaScopedArray<float> dev_src, dev_dst;
    dev_src.allocate(size);
    dev_dst.allocate(size);

    // Copy input vectors from host memory to GPU buffers
    dev_src.uploadToDevice(source);

    // Calculate optimal block size to maximize GPU occupancy
    determineBlockGridSize(device, 1, N, gridSize, blockSize);
    fprintf(stderr, "Optimal block size = %d, grid size = %d\n", blockSize, gridSize);
    // Launch a kernel on the GPU with one thread for each element
    if (shared)
    {
      //inclusiveScanKernelShared<<<gridSize, blockSize>>>(dev_src, dev_dst, N);
    }
    else
    {
      inclusiveScanKernel<<<gridSize, blockSize>>>(dev_src, dev_dst, N);
    }

    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    THROW_ON_CUDA_ERROR(
      cudaDeviceSynchronize()
    );

    // Copy output vector from GPU buffer to host memory.
    dev_dst.uploadFromDevice(result);
  }
  catch (thrust::system_error& e)
  {
    std::cerr << "CUDA error: " << e.what() << std::endl;
    return cudaError(-1);
  }

  return cudaSuccess;
}
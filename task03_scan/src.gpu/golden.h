#pragma once

// Inclusive scan
inline void process_golden(float *result, const float *source, int N)
{
	result[0] = source[0];

  for (int i = 1; i < N; ++i)
  {
		result[i] = result[i - 1] + source[i];
  }
}
#pragma once



//#define TILE_W 5
//
//__global__ void convolution2DKernelShared(const float *src, float *dst, int N, int M)
//{
//  extern __shared__ float s_data[];
//
//  int i = blockIdx.x * blockDim.x + threadIdx.x;
//  int y = i / N, x = i - y * N;
//  int R = M / 2;
//
//  float sum = 0;
//  for (int dy = -R; dy <= R; ++dy)
//  {
//    for (int dx = -R; dx <= R; ++dx)
//    {
//      float offset_value;
//      int neib_x = x + dx, neib_y = y + dy;
//
//      if (neib_x < 0 || neib_x >= N || neib_y < 0 || neib_y >= N)
//        offset_value = 0;
//      else
//        offset_value = src[neib_y * N + neib_x];
//
//      int kernel_idx = (dy + R) * R + (dx + R);
//
//      sum += offset_value * c_kernel[kernel_idx];
//    }
//  }
//
//  dst[i] = sum;
//}
#pragma warning(disable:4996)

#include <iostream>
#include <iterator>
#include <sstream>
#include <vector>
#include <algorithm>
#include <functional>

#include <cstdio>
#include <cstdlib>
#include <cassert>

#include <cuda_runtime.h>
// Required to include CUDA vector types
//#include <vector_types.h>

using namespace std;

// forward declaration of helper function
extern "C" cudaError_t process(const int *a, int *b, size_t m, size_t n);

template <typename T>
void read_values(const string &s, vector<T> &values)
{
	istringstream iss;
	iss.str(s);
	copy(istream_iterator<T>(iss), istream_iterator<T>(), back_inserter(values));
}

template <typename T>
void print_matrix(const vector<T> &values, int M, int N, const string &name)
{
	cerr << "###########################" << endl;
	cerr << "### " << name << endl;
	cerr << "###########################" << endl;

	for (int i = 0; i < M; ++i)
	{
		for (int j = 0; j < N; ++j)
		{
			cerr << values[i * N + j] << ' ';
		}
		cerr << endl;
	}
}


void gen(int M, int N)
{
  std::ostringstream ss;
  ss << "input_" << M << "x" << N << ".txt";
  string fileName = ss.str();

  freopen(fileName.c_str(), "w", stdout);

  cout << M << " " << N << endl;

  for (int i = 0; i < M; ++i)
  {
    for (int j = 0; j < N; ++j)
    {
      //cout << ((rand() % 2) ? "1 " : "0 ");
      //bool c = bool(cout);
      cout << rand() % 2 ? "1" : "0";
      cout << " ";
    }

    cout << endl;
  }
}


int main(int argc, char **argv)
{
	typedef vector<float> Vecf;
	typedef vector<int> Veci;
	Veci values;
	string s;
	int M, N;

  //gen(1024, 1024);
  //return 0;

	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	// read input life matrix dimensions
	getline(cin, s);
	read_values(s, values);
	assert(values.size() == 2);
	M = values[0];
	N = values[1];
	values.clear();

	// ghost row construction
	int ghostRowSize = N + 2;
	int *ghostRow = new int [ghostRowSize];
	fill(&ghostRow[0], &ghostRow[ghostRowSize], 0);
	// append ghost row of 0s to the beginning
	values.insert(values.end(), &ghostRow[0], &ghostRow[ghostRowSize]);

	// read input life matrix values, row by row
	for (int i = 0; i < M; ++i)
	{
		values.push_back(0);
		getline(cin, s);
		read_values(s, values);
		values.push_back(0);

		assert(values.size() == (i + 2) * (N + 2));
	}

	// append last row of 0s
	values.insert(values.end(), &ghostRow[0], &ghostRow[ghostRowSize]);

  if (M * N < 100)
	  print_matrix(values, M + 2, N + 2, "Input");

	// create resulting array of same size
	Veci result(values);

	// run the device part of the program
	cudaError_t cudaStatus = process(values.data(), result.data(), M + 2, N + 2);
	if (cudaStatus != cudaSuccess)
	{
		cerr << "cuda execution failed!" << endl;
		return 1;
	}

  if (M * N < 100)
	  print_matrix(result, M + 2, N + 2, "Result");

	// write output life matrix
	for (int i = 1; i <= M; ++i)
	{
		for (int j = 1; j <= N; ++j)
		{
			cout << result[i * (N + 2) + j] << ' ';
		}
		cout << endl;
	}


	// cudaDeviceReset must be called before exiting in order for profiling and
	// tracing tools such as Nsight and Visual Profiler to show complete traces.
	cudaStatus = cudaDeviceReset();
	if (cudaStatus != cudaSuccess)
	{
		cerr << "cudaDeviceReset failed!" << endl;
		return 1;
	}

	return 0;
}

// Cuda headers
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
// Helper library
#include "helper_cuda.h"

#include <cstdio>
#include <cassert>


// TODO: pitched memory
//+TODO: barriers (n+1..old_arr..n+1)
//+TODO: automatically choose the most powerful CUDA device

typedef unsigned int uint;

__global__ void lifeKernel(const int *a, int *b, int m, int n)
{
  int i = blockIdx.x * blockDim.x + threadIdx.x;

  // Check that we're fitting into memory boundaries
  if (i < (n + 1) || i >= m*n - (n + 1)) {
    //b[i] = -1;
    return;
  }

  int self = a[i];
  int neib = a[i - n - 1] + a[i - n] + a[i - n + 1] + 
             a[i - 1]     + /*a[i]*/ + a[i + 1] +
             a[i + n - 1] + a[i + n] + a[i + n + 1];

  int raiseDead = (self == 0 && (neib == 3));
  int killHim   = (self == 1 && (neib > 3 || neib < 2));
  b[i] = a[i] + raiseDead - killHim;

  // Version with if's
  //b[i] = a[i];
  //if (self == 1 && (neib > 3 || neib < 2))
  //	b[i] = 0;
  //else if (self == 0 && (neib == 3))
  //	b[i] = 1;
}


cudaDeviceProp getDeviceProperties(int device)
{
  cudaDeviceProp deviceProp;
  cudaGetDeviceProperties(&deviceProp, device);

  fprintf(stderr, "Device %d: \"%s\"\n", device, deviceProp.name);

  int driverVersion, runtimeVersion;
  cudaDriverGetVersion(&driverVersion);
  cudaRuntimeGetVersion(&runtimeVersion);
  fprintf(stderr, "  CUDA Driver Version / Runtime Version          %d.%d / %d.%d\n", driverVersion/1000, (driverVersion%100)/10, runtimeVersion/1000, (runtimeVersion%100)/10);
  fprintf(stderr, "  CUDA Capability Major/Minor version number:    %d.%d\n", deviceProp.major, deviceProp.minor);

#if 0
  char msg[256];
  _snprintf(msg, 256, "  Total amount of global memory:                 %.0f MBytes (%llu bytes)\n",
    (float)deviceProp.totalGlobalMem/1048576.0f, (unsigned long long) deviceProp.totalGlobalMem);
  fprintf(stderr, "%s", msg);

  fprintf(stderr, "  (%2d) Multiprocessors, (%3d) CUDA Cores/MP:     %d CUDA Cores\n",
    deviceProp.multiProcessorCount,
    _ConvertSMVer2Cores(deviceProp.major, deviceProp.minor),
    _ConvertSMVer2Cores(deviceProp.major, deviceProp.minor) * deviceProp.multiProcessorCount);
  fprintf(stderr, "  GPU Clock rate:                                %.0f MHz (%0.2f GHz)\n", deviceProp.clockRate * 1e-3f, deviceProp.clockRate * 1e-6f);
  fprintf(stderr, "  Maximum Texture Dimension Size (x,y,z)         1D=(%d), 2D=(%d, %d), 3D=(%d, %d, %d)\n",
    deviceProp.maxTexture1D   , deviceProp.maxTexture2D[0], deviceProp.maxTexture2D[1],
    deviceProp.maxTexture3D[0], deviceProp.maxTexture3D[1], deviceProp.maxTexture3D[2]);
  fprintf(stderr, "  Maximum Layered 1D Texture Size, (num) layers  1D=(%d), %d layers\n",
    deviceProp.maxTexture1DLayered[0], deviceProp.maxTexture1DLayered[1]);
  fprintf(stderr, "  Maximum Layered 2D Texture Size, (num) layers  2D=(%d, %d), %d layers\n",
    deviceProp.maxTexture2DLayered[0], deviceProp.maxTexture2DLayered[1], deviceProp.maxTexture2DLayered[2]);
#endif

  fprintf(stderr, "  Total amount of constant memory:               %lu bytes\n", deviceProp.totalConstMem);
  fprintf(stderr, "  Total amount of shared memory per block:       %lu bytes\n", deviceProp.sharedMemPerBlock);
  fprintf(stderr, "  Total number of registers available per block: %d\n", deviceProp.regsPerBlock);
  fprintf(stderr, "  Warp size:                                     %d\n", deviceProp.warpSize);
  fprintf(stderr, "  Maximum number of threads per multiprocessor:  %d\n", deviceProp.maxThreadsPerMultiProcessor);
  fprintf(stderr, "  Maximum number of threads per block:           %d\n", deviceProp.maxThreadsPerBlock);
#if 0
  fprintf(stderr, "  Max dimension size of a thread block (x,y,z): (%d, %d, %d)\n",
    deviceProp.maxThreadsDim[0],
    deviceProp.maxThreadsDim[1],
    deviceProp.maxThreadsDim[2]);
  fprintf(stderr, "  Max dimension size of a grid size    (x,y,z): (%d, %d, %d)\n",
    deviceProp.maxGridSize[0],
    deviceProp.maxGridSize[1],
    deviceProp.maxGridSize[2]);
  fprintf(stderr, "  Maximum memory pitch:                          %lu bytes\n", deviceProp.memPitch);
  fprintf(stderr, "  Texture alignment:                             %lu bytes\n", deviceProp.textureAlignment);
  fprintf(stderr, "  Concurrent copy and kernel execution:          %s with %d copy engine(s)\n", (deviceProp.deviceOverlap ? "Yes" : "No"), deviceProp.asyncEngineCount);
  fprintf(stderr, "  Run time limit on kernels:                     %s\n", deviceProp.kernelExecTimeoutEnabled ? "Yes" : "No");
  fprintf(stderr, "  Integrated GPU sharing Host Memory:            %s\n", deviceProp.integrated ? "Yes" : "No");
  fprintf(stderr, "  Support host page-locked memory mapping:       %s\n", deviceProp.canMapHostMemory ? "Yes" : "No");
  fprintf(stderr, "  Alignment requirement for Surfaces:            %s\n", deviceProp.surfaceAlignment ? "Yes" : "No");
  fprintf(stderr, "  Device has ECC support:                        %s\n", deviceProp.ECCEnabled ? "Enabled" : "Disabled");
  fprintf(stderr, "  Device supports Unified Addressing (UVA):      %s\n", deviceProp.unifiedAddressing ? "Yes" : "No");
  fprintf(stderr, "  Device PCI Bus ID / PCI location ID:           %d / %d\n", deviceProp.pciBusID, deviceProp.pciDeviceID);

  const char *sComputeMode[] =
  {
    "Default (multiple host threads can use ::cudaSetDevice() with device simultaneously)",
    "Exclusive (only one host thread in one process is able to use ::cudaSetDevice() with this device)",
    "Prohibited (no host thread can use ::cudaSetDevice() with this device)",
    "Exclusive Process (many threads in one process is able to use ::cudaSetDevice() with this device)",
    "Unknown",
    NULL
  };
  fprintf(stderr, "  Compute Mode:\n");
  fprintf(stderr, "     < %s >\n", sComputeMode[deviceProp.computeMode]);
#endif

  return deviceProp;
}

// Maximum number of resident blocks is a function of cuda compute capability version
int getMaxResidentBlocksPerSM(int minor, int major)
{
  int maxBlocks = 0;

  // See table 12 of Appendix F of CUDA C Programming Guide
  if (major == 1 || major == 2)
    maxBlocks = 8;
  else if (major == 3)
    maxBlocks = 16;
  else {
    fprintf(stderr, "Unknown CUDA compute capability version %d.%d", major, minor);
    assert(major >= 1 && major <= 3);
  }

  fprintf(stderr, "  Maximum number of resident blocks per SM:      %d\n", maxBlocks);
  return maxBlocks;
}

void determineBlockGridSize(int device, uint m, uint n, uint &gridSize, uint &optimalBlockSize)
{
  cudaDeviceProp props = getDeviceProperties(device);

  uint maxThreadsPerSM = props.maxThreadsPerMultiProcessor/*    = 1536*/;
  uint maxThreadsPerBlock = props.maxThreadsPerBlock/* = 1024*/;
  uint warpSize = props.warpSize;
  uint maxBlocks = getMaxResidentBlocksPerSM(props.minor, props.major)/* = 8*/;
  uint size = m * n;

  uint maxActiveThreads = 0;

  for (uint i = 1, blockSize = warpSize; blockSize < maxThreadsPerBlock; ++i, blockSize = warpSize * i)
  {
    if (blockSize >= size)
    {
      optimalBlockSize = blockSize;
      break;
    }

    int numBlocks = min(maxBlocks, maxThreadsPerSM / blockSize);
    int activeThreads = numBlocks * blockSize;

    if (activeThreads > maxActiveThreads)
    {
      maxActiveThreads = activeThreads;
      optimalBlockSize = blockSize;
    }
  }

  // ceil: round up to the nearest integer
  gridSize = int(ceil(size / float(optimalBlockSize)));
}

void freeGPUBuffs(int *dev_a, int *dev_b)
{
  cudaFree(dev_a);
  cudaFree(dev_b);
}

// Helper function for using CUDA to add vectors in parallel.
extern "C" cudaError_t process(const int *a, int *b, size_t m, size_t n)
{
  int *dev_a = 0;
  int *dev_b = 0;
  cudaError_t cudaStatus;
  uint blockSize, gridSize;
  size_t size = m * n;

  // Get CUDA devices count
  int deviceCount;
  cudaStatus = cudaGetDeviceCount(&deviceCount);

  if (cudaStatus != cudaSuccess) {
    fprintf(stderr, "cudaGetDeviceCount returned %d\n-> %s\n", (int)cudaStatus, cudaGetErrorString(cudaStatus));
    return cudaStatus;
  }

  if (deviceCount == 0) {
    fprintf(stderr, "There are no available device(s) that support CUDA\n");
    return cudaErrorDevicesUnavailable;
  }
  else {
    fprintf(stderr, "Detected %d CUDA Capable device(s)\n", deviceCount);
  }

  // Choose which GPU to run on
  int device = gpuGetMaxGflopsDeviceId();
  cudaStatus = cudaSetDevice(device);
  if (cudaStatus != cudaSuccess) {
    fprintf(stderr, "cudaSetDevice failed! Do you have a CUDA-capable GPU installed?");
    return cudaStatus;
  }

  // Allocate GPU buffers for two vectors (one input, one output)    .
  cudaStatus = cudaMalloc((void**)&dev_b, size * sizeof(int));
  if (cudaStatus != cudaSuccess) {
    fprintf(stderr, "cudaMalloc failed!");
    freeGPUBuffs(dev_a, dev_b);
    return cudaStatus;
  }
  cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(int));
  if (cudaStatus != cudaSuccess) {
    fprintf(stderr, "cudaMalloc failed!");
    freeGPUBuffs(dev_a, dev_b);
    return cudaStatus;
  }

  // Copy input vectors from host memory to GPU buffers.
  cudaStatus = cudaMemcpy(dev_a, a, size * sizeof(int), cudaMemcpyHostToDevice);
  if (cudaStatus != cudaSuccess) {
    fprintf(stderr, "cudaMemcpy failed!");
    freeGPUBuffs(dev_a, dev_b);
    return cudaStatus;
  }
  
  // Calculate optimal block size to maximize GPU occupancy
  determineBlockGridSize(device, m, n, gridSize, blockSize);
  fprintf(stderr, "Optimal block size = %d, grid size = %d\n", blockSize, gridSize);
  // Launch a kernel on the GPU with one thread for each element.
  lifeKernel<<<gridSize, blockSize>>>(dev_a, dev_b, m, n);

  // cudaDeviceSynchronize waits for the kernel to finish, and returns
  // any errors encountered during the launch.
  cudaStatus = cudaDeviceSynchronize();
  if (cudaStatus != cudaSuccess) {
    fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
    freeGPUBuffs(dev_a, dev_b);
    return cudaStatus;
  }

  // Copy output vector from GPU buffer to host memory.
  cudaStatus = cudaMemcpy(b, dev_b, size * sizeof(int), cudaMemcpyDeviceToHost);
  if (cudaStatus != cudaSuccess) {
    fprintf(stderr, "cudaMemcpy failed!");
    freeGPUBuffs(dev_a, dev_b);
    return cudaStatus;
  }

  return cudaStatus;
}
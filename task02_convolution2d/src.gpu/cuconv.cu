// My own cuda helper library
#include "gpulib.h"
// Cuda kernels implementation
#include "kernel.h"

// Cuda entry point for convolution
extern "C" cudaError_t process(float* result, const float* source, const float *kernel, int N, int M, bool shared)
{
  uint blockSize, gridSize;
  size_t size = N * N;

  try
  {
    // Get CUDA devices count
    int deviceCount;
    THROW_ON_CUDA_ERROR(
      cudaGetDeviceCount(&deviceCount)
    );

    if (deviceCount == 0)
    {
      fprintf(stderr, "There are no available device(s) that support CUDA\n");
      return cudaErrorDevicesUnavailable;
    }
    else
    {
      fprintf(stderr, "Detected %d CUDA Capable device(s)\n", deviceCount);
    }

    // Choose which GPU to run on
    int device = gpuGetMaxGflopsDeviceId();
    THROW_ON_CUDA_ERROR(
      cudaSetDevice(device)
    );

    // Allocate GPU buffers for two vectors (one input, one output)
    CudaScopedArray<float> dev_src, dev_dst;
    dev_src.allocate(size);
    dev_dst.allocate(size);

    // Copy input vectors from host memory to GPU buffers
    dev_src.uploadToDevice(source);

    // Upload convolution kernel to constant memory
    THROW_ON_CUDA_ERROR(
      cudaMemcpyToSymbol(c_kernel, kernel, M * M * sizeof(float))
    );
  
    // Calculate optimal block size to maximize GPU occupancy
    determineBlockGridSize(device, N, N, gridSize, blockSize);
    fprintf(stderr, "Optimal block size = %d, grid size = %d\n", blockSize, gridSize);
    // Launch a kernel on the GPU with one thread for each element
    if (shared)
    {
      convolution2DKernelShared<<<gridSize, blockSize>>>(dev_src, dev_dst, N, M);
    }
    else
    {
      convolution2DKernel<<<gridSize, blockSize>>>(dev_src, dev_dst, N, M);
    }

    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    THROW_ON_CUDA_ERROR(
      cudaDeviceSynchronize()
    );

    // Copy output vector from GPU buffer to host memory.
    dev_dst.uploadFromDevice(result);
  }
  catch (thrust::system_error& e)
  {
    std::cerr << "CUDA error: " << e.what() << std::endl;
    return cudaError(-1);
  }

  return cudaSuccess;
}
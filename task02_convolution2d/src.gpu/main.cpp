#pragma warning(disable:4996)

#include <iostream>
#include <iterator>
#include <sstream>
#include <vector>
#include <algorithm>
#include <functional>

#include <cstdio>
#include <cstdlib>
#include <cassert>

#include <cuda_runtime.h>
// Required to include CUDA vector types
//#include <vector_types.h>

// Golden CPU implementation
#include "golden.h"

using namespace std;

// forward declaration of cuda entry point function
extern "C" cudaError_t process(float* result, const float* source, const float *kernel, int N, int M, bool shared);


template <typename T>
void read_values(const string &s, vector<T> &values)
{
	istringstream iss;
	iss.str(s);
	copy(istream_iterator<T>(iss), istream_iterator<T>(), back_inserter(values));
}

template <typename T>
void print_matrix(const vector<T> &values, int M, int N, const string &name)
{
	cerr << "###########################" << endl;
	cerr << "### " << name << endl;
	cerr << "###########################" << endl;

	for (int i = 0; i < M; ++i)
	{
		for (int j = 0; j < N; ++j)
		{
			cerr << values[i * N + j] << ' ';
		}
		cerr << endl;
	}
}


//void gen(int M, int N)
//{
//  std::ostringstream ss;
//  ss << "input_" << M << "x" << N << ".txt";
//  string fileName = ss.str();
//
//  freopen(fileName.c_str(), "w", stdout);
//
//  cout << M << " " << N << endl;
//
//  for (int i = 0; i < M; ++i)
//  {
//    for (int j = 0; j < N; ++j)
//    {
//      //cout << ((rand() % 2) ? "1 " : "0 ");
//      //bool c = bool(cout);
//      cout << rand() % 2 ? "1" : "0";
//      cout << " ";
//    }
//
//    cout << endl;
//  }
//}


int main(int argc, char **argv)
{
	typedef vector<float> Vecf;
	typedef vector<int>   Veci;
	Vecf source, kernel;
	string s;
	int N, M;

  //gen(1024, 1024);
  //return 0;

	if (freopen("input.txt", "r", stdin) == nullptr)
  {
    cout << "No input file found, exiting now." << endl;
    return 1;
  }
	freopen("output.txt", "w", stdout);

	// read input matrices dimensions
	getline(cin, s);
  Veci dimensions;
	read_values(s, dimensions);
	assert(dimensions.size() == 2);
  // source matrix size
	N = dimensions[0];
  // convolution kernel size
	M = dimensions[1];

	//// ghost row construction
	//int ghostRowSize = N + 2;
	//int *ghostRow = new int [ghostRowSize];
	//fill(&ghostRow[0], &ghostRow[ghostRowSize], 0);
	//// append ghost row of 0s to the beginning
	//values.insert(values.end(), &ghostRow[0], &ghostRow[ghostRowSize]);

	// read source matrix values, row by row
	for (int i = 0; i < N; ++i)
	{
		//values.push_back(0);
		getline(cin, s);
		read_values(s, source);
		//values.push_back(0);

		//assert(values.size() == (i + 2) * (N + 2));
	}

	// append last row of 0s
	//values.insert(values.end(), &ghostRow[0], &ghostRow[ghostRowSize]);

  // read kernel matrix values, row by row
  for (int i = 0; i < M; ++i)
  {
    getline(cin, s);
    read_values(s, kernel);
    assert(kernel.size() == (i + 1) * M);
  }

  if (N < 10)
	  print_matrix(source, N, N, "Source");

  print_matrix(kernel, M, M, "Kernel");

	// create golden resulting array of same size
	Vecf result_golden(source);

  // run golden implementation
  process_golden(result_golden.data(), source.data(), kernel.data(), N, M);

  print_matrix(result_golden, N, N, "Convolution result golden");

  // create resulting array of same size
  Vecf result(source);
	// run the device part of the program
	cudaError_t cudaStatus = process(result.data(), source.data(), result.data(), N, M, true);
	if (cudaStatus != cudaSuccess)
	{
		cerr << "cuda execution failed!" << endl;
		return 1;
	}

  if (N < 10)
	  print_matrix(result, N, N, "Convolution result");

  // compare with golden
  // also may use std::mismatch()
  if (! std::equal(result_golden.begin(), result_golden.end(), result.begin()))
  {
    cerr << "VERY BAD ERROR!!! CUDA IMPLEMENTATION IS INVALID!!!" << endl;
  }

	// write output matrix
	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j < N; ++j)
		{
			cout << result[i * N + j] << ' ';
		}
		cout << endl;
	}


	// cudaDeviceReset must be called before exiting in order for profiling and
	// tracing tools such as Nsight and Visual Profiler to show complete traces.
	cudaStatus = cudaDeviceReset();
  if (cudaStatus != cudaSuccess)
  {
    cerr << "cudaDeviceReset failed!" << endl;
    return 1;
  }

	return 0;
}

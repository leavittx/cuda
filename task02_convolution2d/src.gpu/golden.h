#pragma once

inline void process_golden(float *result, const float *source, const float *kernel, int N, int M)
{
  int R = M / 2;

  for (int y = 0; y < N; ++y)
  {
    for (int x = 0; x < N; ++x)
    {
      float sum = 0;
      int idx = y * N + x;

      for (int ry = -R; ry <= R; ++ry)
      {
        for (int rx = -R; rx <= R; ++rx)
        {
          int kernel_idx = (ry + R) * R + (rx + R);
          int offset_x = x + rx, offset_y = y + ry;
          float offset_value;
          if (offset_x < 0 || offset_x >= N || offset_y < 0 || offset_y >= N)
          {
            offset_value = 0;
          }
          else
          {
            offset_value = source[offset_y * N + offset_x];
          }
          //(2R + 1)^2
          //4R^2 + 4R + 1
          //(2R)*R + 2R
          //(2R^2 + 2R)*2 + 1
          sum += offset_value * kernel[kernel_idx];
        }
      }

      result[idx] = sum;
    }
  }
}
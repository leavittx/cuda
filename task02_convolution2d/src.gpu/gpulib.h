#pragma once

// Cuda headers
#include <cuda_runtime.h>

// Thrust
#include <thrust/system_error.h>
#include <thrust/system/cuda/error.h>

// Helper library
#include "helper_cuda.h"

// C library headers
#include <cstdio>
#include <cassert>

// STL
#include <string>
#include <sstream>

// Type definitions
typedef unsigned int uint;

// http://stackoverflow.com/a/14051069/1155958
inline void throw_on_cuda_error(cudaError_t status, const char* file, int line, const char* function = "")
{
  if (status != cudaSuccess)
  {
    std::stringstream ss;
    ss << file << "(" << line << "): ";
    std::string file_and_line;
    ss >> file_and_line;
    throw thrust::system_error(status, thrust::cuda_category(), file_and_line + " " + function);
  }
}

#define THROW_ON_CUDA_ERROR(code) throw_on_cuda_error(code, __FILE__, __LINE__, #code)

template <typename T>
class CudaScopedArray
{
public:
  CudaScopedArray()
    : mPtr(nullptr)
  {

  }
  ~CudaScopedArray()
  {
    cudaFree(mPtr);
  }

  void allocate(size_t count)
  {
    // allocate device memory
    THROW_ON_CUDA_ERROR(
      cudaMalloc((void **)&mPtr, count * sizeof(T))
    );
    // initialize device memory
    THROW_ON_CUDA_ERROR(
      cudaMemset(mPtr, 0, count * sizeof(T))
    );
    mCount = count;
  }

  T* ptr() const
  {
    return mPtr;
  }

  void uploadToDevice(const T* src, size_t count = 0)
  {
    size_t elementCount;

    if (count == 0)
    {
      elementCount = mCount;
    }
    else
    {
      elementCount = count;
    }

    THROW_ON_CUDA_ERROR(
      cudaMemcpy(ptr(), src, elementCount * sizeof(T), cudaMemcpyHostToDevice)
    );
  }

  void uploadFromDevice(T* dest, size_t count = 0)
  {
    size_t elementCount;

    if (count == 0)
    {
      elementCount = mCount;
    }
    else
    {
      elementCount = count;
    }

    THROW_ON_CUDA_ERROR(
      cudaMemcpy(dest, ptr(), elementCount * sizeof(T), cudaMemcpyDeviceToHost)
    );
  }

  operator T* ()
  {
    return ptr();
  }

private:
  T*     mPtr;
  size_t mCount;
};

namespace
{
  inline cudaDeviceProp getDeviceProperties(int device)
  {
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, device);

    fprintf(stderr, "Device %d: \"%s\"\n", device, deviceProp.name);

    int driverVersion, runtimeVersion;
    cudaDriverGetVersion(&driverVersion);
    cudaRuntimeGetVersion(&runtimeVersion);
    fprintf(stderr, "  CUDA Driver Version / Runtime Version          %d.%d / %d.%d\n", driverVersion/1000, (driverVersion%100)/10, runtimeVersion/1000, (runtimeVersion%100)/10);
    fprintf(stderr, "  CUDA Capability Major/Minor version number:    %d.%d\n", deviceProp.major, deviceProp.minor);

  #if 0
    char msg[256];
    _snprintf(msg, 256, "  Total amount of global memory:                 %.0f MBytes (%llu bytes)\n",
      (float)deviceProp.totalGlobalMem/1048576.0f, (unsigned long long) deviceProp.totalGlobalMem);
    fprintf(stderr, "%s", msg);

    fprintf(stderr, "  (%2d) Multiprocessors, (%3d) CUDA Cores/MP:     %d CUDA Cores\n",
      deviceProp.multiProcessorCount,
      _ConvertSMVer2Cores(deviceProp.major, deviceProp.minor),
      _ConvertSMVer2Cores(deviceProp.major, deviceProp.minor) * deviceProp.multiProcessorCount);
    fprintf(stderr, "  GPU Clock rate:                                %.0f MHz (%0.2f GHz)\n", deviceProp.clockRate * 1e-3f, deviceProp.clockRate * 1e-6f);
    fprintf(stderr, "  Maximum Texture Dimension Size (x,y,z)         1D=(%d), 2D=(%d, %d), 3D=(%d, %d, %d)\n",
      deviceProp.maxTexture1D   , deviceProp.maxTexture2D[0], deviceProp.maxTexture2D[1],
      deviceProp.maxTexture3D[0], deviceProp.maxTexture3D[1], deviceProp.maxTexture3D[2]);
    fprintf(stderr, "  Maximum Layered 1D Texture Size, (num) layers  1D=(%d), %d layers\n",
      deviceProp.maxTexture1DLayered[0], deviceProp.maxTexture1DLayered[1]);
    fprintf(stderr, "  Maximum Layered 2D Texture Size, (num) layers  2D=(%d, %d), %d layers\n",
      deviceProp.maxTexture2DLayered[0], deviceProp.maxTexture2DLayered[1], deviceProp.maxTexture2DLayered[2]);
  #endif

    fprintf(stderr, "  Total amount of constant memory:               %lu bytes\n", deviceProp.totalConstMem);
    fprintf(stderr, "  Total amount of shared memory per block:       %lu bytes\n", deviceProp.sharedMemPerBlock);
    fprintf(stderr, "  Total number of registers available per block: %d\n", deviceProp.regsPerBlock);
    fprintf(stderr, "  Warp size:                                     %d\n", deviceProp.warpSize);
    fprintf(stderr, "  Maximum number of threads per multiprocessor:  %d\n", deviceProp.maxThreadsPerMultiProcessor);
    fprintf(stderr, "  Maximum number of threads per block:           %d\n", deviceProp.maxThreadsPerBlock);
  #if 0
    fprintf(stderr, "  Max dimension size of a thread block (x,y,z): (%d, %d, %d)\n",
      deviceProp.maxThreadsDim[0],
      deviceProp.maxThreadsDim[1],
      deviceProp.maxThreadsDim[2]);
    fprintf(stderr, "  Max dimension size of a grid size    (x,y,z): (%d, %d, %d)\n",
      deviceProp.maxGridSize[0],
      deviceProp.maxGridSize[1],
      deviceProp.maxGridSize[2]);
    fprintf(stderr, "  Maximum memory pitch:                          %lu bytes\n", deviceProp.memPitch);
    fprintf(stderr, "  Texture alignment:                             %lu bytes\n", deviceProp.textureAlignment);
    fprintf(stderr, "  Concurrent copy and kernel execution:          %s with %d copy engine(s)\n", (deviceProp.deviceOverlap ? "Yes" : "No"), deviceProp.asyncEngineCount);
    fprintf(stderr, "  Run time limit on kernels:                     %s\n", deviceProp.kernelExecTimeoutEnabled ? "Yes" : "No");
    fprintf(stderr, "  Integrated GPU sharing Host Memory:            %s\n", deviceProp.integrated ? "Yes" : "No");
    fprintf(stderr, "  Support host page-locked memory mapping:       %s\n", deviceProp.canMapHostMemory ? "Yes" : "No");
    fprintf(stderr, "  Alignment requirement for Surfaces:            %s\n", deviceProp.surfaceAlignment ? "Yes" : "No");
    fprintf(stderr, "  Device has ECC support:                        %s\n", deviceProp.ECCEnabled ? "Enabled" : "Disabled");
    fprintf(stderr, "  Device supports Unified Addressing (UVA):      %s\n", deviceProp.unifiedAddressing ? "Yes" : "No");
    fprintf(stderr, "  Device PCI Bus ID / PCI location ID:           %d / %d\n", deviceProp.pciBusID, deviceProp.pciDeviceID);

    const char *sComputeMode[] =
    {
      "Default (multiple host threads can use ::cudaSetDevice() with device simultaneously)",
      "Exclusive (only one host thread in one process is able to use ::cudaSetDevice() with this device)",
      "Prohibited (no host thread can use ::cudaSetDevice() with this device)",
      "Exclusive Process (many threads in one process is able to use ::cudaSetDevice() with this device)",
      "Unknown",
      NULL
    };
    fprintf(stderr, "  Compute Mode:\n");
    fprintf(stderr, "     < %s >\n", sComputeMode[deviceProp.computeMode]);
  #endif

    return deviceProp;
  }

  // Maximum number of resident blocks is a function of cuda compute capability version
  inline int getMaxResidentBlocksPerSM(int minor, int major)
  {
    int maxBlocks = 0;

    // See table 12 of Appendix F of CUDA C Programming Guide
    if (major == 1 || major == 2)
      maxBlocks = 8;
    else if (major == 3)
      maxBlocks = 16;
    else {
      fprintf(stderr, "Unknown CUDA compute capability version %d.%d", major, minor);
      assert(major >= 1 && major <= 3);
    }

    fprintf(stderr, "  Maximum number of resident blocks per SM:      %d\n", maxBlocks);
    return maxBlocks;
  }
} // namespace anonymous

inline void determineBlockGridSize(int device, uint m, uint n, uint &gridSize, uint &optimalBlockSize)
{
  cudaDeviceProp props = getDeviceProperties(device);

  uint maxThreadsPerSM = props.maxThreadsPerMultiProcessor/*    = 1536*/;
  uint maxThreadsPerBlock = props.maxThreadsPerBlock/* = 1024*/;
  uint warpSize = props.warpSize;
  uint maxBlocks = getMaxResidentBlocksPerSM(props.minor, props.major)/* = 8*/;
  uint size = m * n;

  uint maxActiveThreads = 0;

  for (uint i = 1, blockSize = warpSize; blockSize < maxThreadsPerBlock; ++i, blockSize = warpSize * i)
  {
    if (blockSize >= size)
    {
      optimalBlockSize = blockSize;
      break;
    }

    uint numBlocks = min(maxBlocks, maxThreadsPerSM / blockSize);
    uint activeThreads = numBlocks * blockSize;

    if (activeThreads > maxActiveThreads)
    {
      maxActiveThreads = activeThreads;
      optimalBlockSize = blockSize;
    }
  }

  // ceil: round up to the nearest integer
  gridSize = int(ceil(size / float(optimalBlockSize)));
}
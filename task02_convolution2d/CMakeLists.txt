# http://www.cmake.org/Wiki/CMake:Install_Commands#Replace_INSTALL_TARGETS
# http://www.cmake.org/Wiki/CMake_Useful_Variables

cmake_minimum_required(VERSION 2.8)

project(cuconv)

# set(CMAKE_MODULE_PATH "/usr/share/cmake/Modules" ${CMAKE_MODULE_PATH})

find_package(CUDA)
#find_package(CUDA REQUIRED)

if (CUDA_FOUND)
        message("CUDA found, using device squaring!")
        add_subdirectory(src.gpu)
else()
        message("CUDA not found, doing something alternatively")
        add_subdirectory(src.cpu)
endif()


#set(CMAKE_CXX_FLAG "-g -Wall")

INSTALL(FILES input.txt DESTINATION bin)


